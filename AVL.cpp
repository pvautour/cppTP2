#include <iostream> // a retirer; pour tests
#include "AVL.h"

ArbreAVL::ArbreAVL() {

}

bool ArbreAVL::inserer(Noeud * & n, const string & element) {

  if (n == NULL) {
    //creation d’une nouvelle feuille
    n = new Noeud(element, NULL, NULL);
    return true; // la hauteur a augmentee dans cette feuille
  }
  if (element < n->contenu) {
    // cas de gauche
    if (inserer(n->gauche, element)) {
      n->equilibre++; // le sous-arbre n->gauche a grandi en hauteur
      if (n->equilibre == 0) return false; // sous-arbre n meme hauteur
      if (n->equilibre == 1) return true; // sous-arbre n a grandi
      // reequilibrage requis
      if (n->gauche->equilibre == -1) // cas double rotation
        rotationDroiteGauche(n->gauche);
      rotationGaucheDroite(n); // rotation pour equilibrer n
    }
    return false;
  } else if (n->contenu < element) { // equivalant if(element>n->contenu)
    /* le cas de droite est laisse en exercice */
    // cas de droite
    if (inserer(n->droite, element)) {
      n->equilibre--; // le sous-arbre n->droite a grandi en hauteur <----- a verifier$$$$$$$$$$$$
      if (n->equilibre == 0) return false; // sous-arbre n meme hauteur
      if (n->equilibre == -1) return true; // sous-arbre n a grandi
      // reequilibrage requis
      if (n->droite->equilibre == 1) // cas double rotation
        rotationGaucheDroite(n->droite);
      rotationDroiteGauche(n); // rotation pour equilibrer n
    }
    return false;

  } else { // equivalant if(n->contenu == element)
    n->incr();    // Incrementation du nombre d'occurence du mot
    return false; // le sous-arbre n conserve la meme hauteur
  }

  return false;
}

void ArbreAVL::inserer(const string & element) {
  inserer(racine, element);
}

int ArbreAVL::getRepetitions(Noeud * & n, const string & element){
  if(n == NULL) {
    return 0;
  }

  if(element < n->contenu) {
    return getRepetitions(n->gauche, element);    
  }

  if(element > n->contenu) {
    return getRepetitions(n->droite, element);
  }
	
  return n->repetitions;
}

int ArbreAVL::getRepetitions(const string & element) {
  return getRepetitions(racine, element);
}


void ArbreAVL::rotationGaucheDroite(Noeud * & racinesousarbre) {
  Noeud * a = racinesousarbre->gauche;
  Noeud * b = racinesousarbre;
  int ea = a->equilibre;
  int eb = b->equilibre;
  int ebp = -(ea > 0 ? ea : 0) - 1 + eb;
  int eap = ea + (ebp < 0 ? ebp : 0) - 1;

  a->equilibre = eap;
  b->equilibre = ebp;
  b->gauche = a->droite;
  a->droite = b;
  racinesousarbre = a;
}

void ArbreAVL::rotationDroiteGauche(Noeud * & racinesousarbre) {
  Noeud * a = racinesousarbre->droite;
  Noeud * b = racinesousarbre;
  int ea = a->equilibre;
  int eb = b->equilibre;
  int ebp = -(ea > 0 ? ea : 0) - 1 + eb;
  int eap = ea + (ebp < 0 ? ebp : 0) - 1;

  a->equilibre = eap;
  b->equilibre = ebp;
  b->droite = a->gauche;
  a->gauche = b;
  racinesousarbre = a;
}
